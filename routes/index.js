require('../helpers/QueryVerificator');

__requestedSite__ = "https://www.metaweather.com";
var express = require('express');
var router = express.Router();
var rest = require('rest');

router.get('/', function(req, res, next) {
    var query = req.query;
    var city = query.weathercity;
    var email = query.email;

    verifyQuery(city, email, res);

    var id = null;
    var cityMoniker = null;
    cityMoniker = city;
    if (city === "NewYork"){ cityMoniker = "york"}

    rest(__requestedSite__+'/api/location/search/?query='+cityMoniker).then(function(response) {
        id = JSON.parse(response.entity)[0].woeid;
        if (id == null) {
            return
        }

        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1;
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        rest(__requestedSite__ + '/api/location/'+id+'/'+year+'/'+month+'/'+day+'/').then(function(response) {
            var lastHourWeather = JSON.parse(response.entity)[0];
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(lastHourWeather));
        });
    });
});

module.exports = router;
