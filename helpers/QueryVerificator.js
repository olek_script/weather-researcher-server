__emailRegExpString__ = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
__requiredCities__ = ["NewYork", "London"];
var emailPattern = new RegExp(__emailRegExpString__);

verifyQuery = function(city, email, res){
    if (!city || !email) {
        res.status(400);
        res.send("Request should contain 'weathercity' and 'email' params");
        return
    }
    if (!(__requiredCities__.indexOf(city) > -1)) {
        res.status(400);
        res.send("Works only for New York and London!");
        return
    }
    if (!emailPattern.test(email)){
        res.status(400);
        res.send("Entered incorrect email address");
        return
    }
};

module.exports = verifyQuery;
